#!/usr/bin/env python

import pbkdf2
import random


def test():
    msg1 = bytearray(get_random_bytes())
    passw = bytearray(get_random_bytes_s(16))

    second_pbkdf2 = pbkdf2.pbkdf2_custom(passw, msg1, 30, 52)

def get_random_bytes():
    """Get Random Bits
    Generates a sequence of random bits of a random size between 1 and 1000
    bits in the sequence.
    Returns:
        A stream of random bits.
    """
    size = random.randrange(1, 1000)

    for _ in range(size):
        yield random.getrandbits(8)


def get_random_bytes_s(size):
    """Get Random Bits
    Generates a sequence of random bits of a random size between 1 and 1000
    bits in the sequence.
    Returns:
        A stream of random bits.
    """
    for _ in range(size):
        yield random.getrandbits(8)


if __name__ == '__main__':
    test()


