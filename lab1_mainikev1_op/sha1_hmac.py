import sha1


BLOCK_SIZE_SHA_1_BYTES = 64
BLOCK_SIZE_SHA_1_BITS = BLOCK_SIZE_SHA_1_BYTES * 8
IPAD = bytearray(b'\x36')*64
OPAD = bytearray(b'\x5c')*64


def new(key, message): # key : bytes,  message: bytes, bytearray, memoryview
    if len(key) < BLOCK_SIZE_SHA_1_BYTES:
        # Step 1. Padding for key zero bytes to block size
        zero_padding = bytearray(BLOCK_SIZE_SHA_1_BYTES - len(key))
        key = key + zero_padding

    # Step 2. KEY Xor IPAD
    key_xor_ipad = bytes(a ^ b for (a, b) in zip(key, IPAD))
    # Step 3. Concat message
    key_xor_ipad = key_xor_ipad + message

    # Step 4. Hash function SHA-1
    hashed = sha1.sha1(key_xor_ipad)

    # Step 5. KEY Xor OPAD
    key_xor_opad = bytes(a ^ b for (a, b) in zip(key, OPAD))
    # Step 6. KEY concat previous hash
    key_xor_opad = key_xor_opad + bytes.fromhex(hashed)

    # Step 7. Hash function SHA-1
    result_hash = sha1.sha1(key_xor_opad)

    return bytes.fromhex(result_hash)


def new_f_hash(key): # key : bytes,  message: bytes, bytearray, memoryview
    if len(key) < BLOCK_SIZE_SHA_1_BYTES:
        # Step 1. Padding for key zero bytes to block size
        zero_padding = bytearray(BLOCK_SIZE_SHA_1_BYTES - len(key))
        key = key + zero_padding

    # Step 2. KEY Xor IPAD
    key_xor_ipad = bytes(a ^ b for (a, b) in zip(key, IPAD))

    # Step 4. Hash function SHA-1
    _h_1 = sha1.sha1_f(key_xor_ipad)

    # Step 5. KEY Xor OPAD
    key_xor_opad = bytes(a ^ b for (a, b) in zip(key, OPAD))

    # Step 7. Hash function SHA-1
    _h_2 = sha1.sha1_f(key_xor_opad)

    return (_h_1, _h_2)


def new_trunc(message, first_hash, second_hash): # key : bytes,  message: bytes, bytearray, memoryview

    # Step 4. Hash function SHA-1
    hashed = sha1.sha1_trunc(message, first_hash)

    # Step 7. Hash function SHA-1
    result_hash = sha1.sha1_trunc(bytes.fromhex(hashed), second_hash)

    return bytes.fromhex(result_hash)




