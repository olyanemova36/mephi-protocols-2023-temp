import math

import sha1_hmac
import hmac

h_len = 20 # BYTES


def func_custom(p, s, c, i):  # p - password, s - salt, c - count of iter, i = integer
    result = []
    u_i = []
    _h_1, _h_2 = sha1_hmac.new_f_hash(p)
    for k in range(c):
        if k == 0:
            u_i = sha1_hmac.new_trunc(s + bytearray([i]), _h_1, _h_2)
        else:
            u_i = sha1_hmac.new_trunc(u_i, _h_1, _h_2)
        result += u_i

    return result


def func(p, s, c, i):  # p - password, s - salt, c - count of iter, i = integer
    result = []
    u_i = []
    for k in range(c):
        if k == 0:
            u_i = hmac.new(p, s + bytearray([i]), "sha1").digest()
        else:
            u_i = hmac.new(p, u_i, "sha1").digest()
        result += u_i

    return result


def pbkdf2(p, s, c, dk_len):
    l = math.ceil(dk_len/h_len)
    r = dk_len - (l-1)*h_len

    result = []

    for j in range(l):
        t_j = func(p, s, c, j)
        result += t_j

    return result[:dk_len]


def pbkdf2_custom(p, s, c, dk_len):
    l = math.ceil(dk_len / h_len)
    r = dk_len - (l - 1) * h_len

    result = []

    for j in range(l):
        t_j = func_custom(p, s, c, j)
        result += t_j

    return result[:dk_len]


