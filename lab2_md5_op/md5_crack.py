import md5_crack
import md5_inverse


def generate_messages(length):
    res = []
    for i in range(2**length):
        b = bytearray()
        while i:
            b.append(i & 0xff)
            i >>= 8
        res.append(bytes(b[::-1]))
    return res


def earlier_prediction_attack(expected_hash):
    for m_fixes in generate_messages(480): # length in bytes
        pre_hash = md5_inverse.md5_rollback_till(bytes.fromhex(expected_hash), 45)
        for m_0 in generate_messages(32):  # length in bytes
            if md5_inverse.md5_till(m_fixes, 48) == pre_hash:
                print('Password recovered! Password : ', bytes.hex(m_0 + m_fixes))
                return
    print('The password was not found during the enumeration.')


def process(arguments):
    if md5_crack.is_correct_mask(arguments.mask):
        earlier_prediction_attack(arguments.hash)
    else:
        print("Incorrect mask wildcards!")
        raise Exception("Incorrect wildcards was provided in mask. Available are: \"a\", \"d\", \"l\", \"u\".")


if __name__ == '__main__':
    process(md5_crack.create_argument_parser().parse_args())
