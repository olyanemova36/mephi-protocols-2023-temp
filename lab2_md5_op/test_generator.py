import argparse

import md5


def process(args):
    password_in_bytes = bytes(args.passw, encoding='utf8')
    if len(password_in_bytes) > 56:
        raise Exception("Utility works only with passwords, where length = 56 bytes.")
    else:
        hash = md5.md5(bytes(args.passw, encoding='utf8'))
        print("Calculated hash: ", bytes.hex(hash))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('-p',
                        '--passw',
                        action='store',
                        type=str,
                        required=True,
                        help='set the password')

    process(parser.parse_args())

