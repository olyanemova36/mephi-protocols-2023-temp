import md5
import md5_inverse
import argparse
import time


def get_count(integer):
    if integer == 0:
        return 0
    elif integer < 2**8:
        return 1
    elif integer < 2**16:
        return 2
    elif integer < 2**24:
        return 3
    elif integer < 2**32:
        return 4
    else:
        return 5


def process(args):
    hash_in_bytes = bytes.fromhex(args.hash)
    if len(hash_in_bytes) != 16:
        print(args.hash)
        print(hash_in_bytes)
        raise Exception("MD5 hash should be 32 bytes length.")
    else:
        if args.mode == 'fast':
            fast_earlier_prediction_attack(hash_in_bytes)
        else:
            basic_brute_force(hash_in_bytes)


def generate_messages(length, bytes_count):
    if length > 0:
        return length.to_bytes(bytes_count, 'little')
    else:
        return bytes()


def fast_earlier_prediction_attack(expected_hash):
    global_counter = 0
    start = time.time()
    for i in range(2**448):  # length in bits
        m_1_15 = generate_messages(i,  get_count(i))
        m_0_dummy = bytes([0x00, 0x00])
        pre_hash = md5_inverse.md5_rollback_till(expected_hash, 45, m_0_dummy + m_1_15)
        for j in range(2**32):  # length in bits
            global_counter += 1
            m_0 = generate_messages(j, get_count(j))
            computed_hash = md5_inverse.md5_till(m_0 + m_1_15, 49)
            if computed_hash == pre_hash:
                end = time.time()
                print(' speed: ' + str(
                    round(global_counter / (end - start))) + ' cand/sc\n')
                print('Password recovered! Password : ', m_0 + m_1_15)
                exit(1)
    print('The password was not found during the enumeration.')


def basic_brute_force(expected_hash):
    global_counter = 0
    start = time.time()
    for i in range(2**448):
        global_counter += 1
        message = generate_messages(i, get_count(i))
        if md5.md5(message) == expected_hash:
            end = time.time()
            print(' speed: ' + str(
                round(global_counter / (end - start))) + ' cand/sc\n')
            print('Password recovered! Password : ', message)
            return
    print('The password was not found during the enumeration.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.version = '1.0'
    parser.add_argument('-v',
                        '--version',
                        action='version')
    parser.add_argument('--hash',
                        action='store',
                        type=str,
                        required=True,
                        help='set the hash for recovery (in hex)')
    parser.add_argument('-m',
                        '--mode',
                        action='store',
                        required=True,
                        type=str,
                        help='set the mode for password recovery',
                        choices=['fast', 'basic'])

    process(parser.parse_args())
