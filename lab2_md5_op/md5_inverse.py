import md5


def right_rotate(x, amount):
    x &= 0xFFFFFFFF
    return ((x >> amount) | (x << (32 - amount)))


def create_chunk(message):  # len(message) <= 64 bytes
    message = bytearray(message)  # copy our input into a mutable buffer
    orig_len_in_bits = (8 * len(message)) & 0xffffffffffffffff
    message.append(0x80)
    while len(message) % 64 != 56:
        message.append(0)
    message += orig_len_in_bits.to_bytes(8, byteorder='little')
    chunk = message[0:64]
    return chunk


def md5_rollback_till(hash, till_number, message):
    a, b, c, d = [hash[:4], hash[4:8], hash[8:12], hash[12:]]

    a = (int.from_bytes(a, byteorder='little') - md5.init_values[0]) & 0xFFFFFFFF
    b = (int.from_bytes(b, byteorder='little') - md5.init_values[1]) & 0xFFFFFFFF
    c = (int.from_bytes(c, byteorder='little') - md5.init_values[2]) & 0xFFFFFFFF
    d = (int.from_bytes(d, byteorder='little') - md5.init_values[3]) & 0xFFFFFFFF

    #print(bytes.hex(a.to_bytes(length=4, byteorder='little')), bytes.hex(b.to_bytes(length=4, byteorder='little')),
          #bytes.hex(c.to_bytes(length=4, byteorder='little')), bytes.hex(d.to_bytes(length=4, byteorder='little')))

    chunk = create_chunk(message)

    for i in range(63, till_number+3, -1):  # begins calculate from Q_59
        from_rotate = right_rotate(b - c, md5.rotate_amounts[i])
        f = md5.functions[i](c, d, a)
        g = md5.index_functions[i](i)
        ch = chunk[4 * g:4 * g + 4]
        previous_a = ((from_rotate - f) - md5.constants[i]) - int.from_bytes(ch, byteorder='little')& 0xFFFFFFFF
        a, b, c, d = previous_a, c, d, a

        a &= 0xFFFFFFFF
        b &= 0xFFFFFFFF
        c &= 0xFFFFFFFF
        d &= 0xFFFFFFFF

        #print(bytes.hex(a.to_bytes(length=4, byteorder='little')), bytes.hex(b.to_bytes(length=4, byteorder='little')), bytes.hex(c.to_bytes(length=4, byteorder='little')), bytes.hex(d.to_bytes(length=4, byteorder='little')))

    a &= 0xFFFFFFFF
    b &= 0xFFFFFFFF
    c &= 0xFFFFFFFF
    d &= 0xFFFFFFFF

    return sum(x << (32 * i) for i, x in enumerate([a, b, c, d])).to_bytes(16, 'little')


def md5_till(message, last_pre_hash_number):  # len(message) <= 64 bytes
    chunk = create_chunk(message)
    hash_pieces = md5.init_values[:]
    a, b, c, d = hash_pieces
    for i in range(last_pre_hash_number):
        f = md5.functions[i](b, c, d)
        g = md5.index_functions[i](i)
        to_rotate = a + f + md5.constants[i] + int.from_bytes(chunk[4 * g:4 * g + 4], byteorder='little')
        new_b = (b + md5.left_rotate(to_rotate, md5.rotate_amounts[i])) & 0xFFFFFFFF
        a, b, c, d = d, new_b, b, c

        #print(bytes.hex(a.to_bytes(length=4, byteorder='little')), bytes.hex(b.to_bytes(length=4, byteorder='little')), bytes.hex(c.to_bytes(length=4, byteorder='little')), bytes.hex(d.to_bytes(length=4, byteorder='little')))

    for i, val in enumerate([a, b, c, d]):
        hash_pieces[i] += val
        hash_pieces[i] &= 0xFFFFFFFF

    return (sum(x << (32 * i) for i, x in enumerate([a, b, c, d]))).to_bytes(16, byteorder='little')
