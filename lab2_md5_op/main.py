from md5 import md5

if __name__ == '__main__':
    demo = [b"", b"a", b"abc", b"message digest", b"abcdefghijklmnopqrstuvwxyz",
            b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
            b"12345678901234567890123456789012345678901234567890123456789012345678901234567890", b"admin123"]
    for message in demo:
        print(bytes.hex(md5(message)), ' <= "', message.decode('ascii'), '"', sep='')

